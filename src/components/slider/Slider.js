import React, { Component } from "react";
import "./slider.css";

class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0 //setting up initial index at 0
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.onRightArrow(); // adding 5 second wait time for slide change
    }, 5000);
  }

  onRightArrow = () => {
    this.state.index === this.props.slides.length - 1
      ? this.setState({ index: 0 }) // infinite loop  go to start of slide when end slide
      : this.setState({ index: this.state.index + 1 }); // move to next slide
  };

  onLeftArrow = () => {
    this.state.index === 0
      ? this.setState({ index: this.props.slides.length - 1 }) // infinite loop go to end of slide when 0
      : this.setState({ index: this.state.index - 1 }); //move to previous slide
  };

  render() {
    const slides = this.props.slides;
    let xDown = null; //initializing y,x Down value for mobile touch
    let yDown = null;
    const handleTouchStart = evt => {
      xDown = evt.touches[0].clientX;
      yDown = evt.touches[0].clientY;
    };

    const handleTouchMove = evt => {
      if (!xDown || !yDown) {
        return;
      }
      //Detecting left or right movement on drag swipe
      let xUp = evt.touches[0].clientX;
      let yUp = evt.touches[0].clientY;
      let xDiff = xDown - xUp;
      let yDiff = yDown - yUp;

      Math.abs(xDiff) > Math.abs(yDiff) && xDiff > 0
        ? this.onRightArrow()
        : this.onLeftArrow();
      //reseting value
      xDown = null;
      yDown = null;
    };
    return (
      <div className="container">
        <div
          className="slider"
          onTouchMove={ev => handleTouchMove(ev)}
          onTouchStart={ev => handleTouchStart(ev)}
        >
          {this.props.slides[this.state.index]}
        </div>

        <i className="arrow-right" onClick={this.onRightArrow} />
        <i className="arrow-left" onClick={this.onLeftArrow} />

        <div className="dots-wrapper">
          <div className="dots">
            {slides.map((slide, index) => {
              return (
                <div
                  className="dot"
                  key={index}
                  style={{
                    backgroundColor: this.state.index === index && "grey" //focus on dot
                  }}
                  onClick={() => {
                    this.setState({ index: index }); //detecting index
                  }}
                ></div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Slider;
