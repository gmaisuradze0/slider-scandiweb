import React from "react";
import "./App.css";
import Slider from "./slider/Slider";
import Test from "./slider/Test";

function App() {
  const slide1 = (
    <img
      style={{ width: "100%", height: "100%" }}
      alt="baby yoda"
      src="https://am23.mediaite.com/tms/cnt/uploads/2020/01/babyyoda-1200x675.jpg"
    />
  );
  const slide2 = (
    <div>
      <h1>H1</h1>
      <h2>H2</h2>
      <h1>H1</h1>
      <h2>H2</h2>
      <h1>H1</h1>
      <h2>H2</h2>
    </div>
  );
  const slide3 = <p>Hello World</p>;
  const slides = [slide1, slide2, slide3, <Test />];
  return (
    <div className="main">
      <Slider slides={slides} />
    </div>
  );
}

export default App;
