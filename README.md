## Packages

Dependencies

1. "react": "^16.13.1",
2. "react-dom": "^16.13.1"

Dev Dependencies

1. "@babel/core": "^7.8.7",
2. "@babel/plugin-proposal-class-properties": "^7.8.3",
3. "@babel/preset-env": "^7.8.7",
4. "@babel/preset-react": "^7.8.3",
5. "babel-loader": "^8.0.6",
6. "css-loader": "^3.4.2",
7. "html-webpack-plugin": "^3.2.0",
8. "style-loader": "^1.1.3",
9. "webpack": "^4.42.0",
10. "webpack-cli": "^3.3.11",
11. "webpack-dev-server": "^3.10.3"

---

## How to use

Cloning repository 

1. run **git clone https://gmaisuradze0@bitbucket.org/gmaisuradze0/slider-scandiweb.git** in terminal.
2. **cd scandi-web**.
3. **npm install**.
4. run **npm start** for dev-server with hot reload or **npm run-script build** for building application.

"scripts": {
    "start": "webpack-dev-server --mode development --open --hot",
    "build": "webpack --mode production"
	}
	
---